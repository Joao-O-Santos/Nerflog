<!-- 2 MENU_ENTRY=Mod Wiki -->
# Mod Wiki



## AR Removal

- Firestrike and Sidestrike: really easy to remove non-destructively.

- Jolt: ar removal is destructive, hammer and chisel method works well.

- Reflex: ar removal is destructive, I think the hammer and chisel
  method works, but I think it takes more effort and clean up
  afterwards.

- Sharpshot: ar removal is destructive. I must try the hammer and
  chisel method. I used a drill bit on a rotary tool and it was a real
  pain to get it done that way.



## Spring Upgrades

- Rampage: I tried fitting a cut down Rotofury spring but couldn't get
  it to catch even after upgrading the catch, don't know if it's a
  matter of it not compressing down enough.

- Rotofury: Takes a Strongarm spring, but that spring is about half the
  size of the Rotofury spring. It's somewhat tricky to disassemble, and
  reassemble the blaster. I made a dumb mistake the last time I was
  reassembling it, and ended up ruining the trigger. I have gutted it
  for parts, cutting down its spring and fitting it to a Firestrike. I
  should try using other springs in the future. Captain Xavier got a k26
  to work, but those springs get expensive to ship to Portugal, and I
  prefer to buy used blasters.

- Firestrike: Takes a Strongarm spring, without needing to upgrade the
  catch, and with noticeable improvements to power and range (I have no
  chronograph to measure the improvement objectively). Takes a cut-down
  Rotofury spring, longer than the original one, less power than the
  Strongarm+Firestrike spring, but much easier prime.

- Sidestrike: The internals are quite similar to the Firestrike.
  I tried adding a Strongarm spring, but it wouldn't catch, even when
  I tried to reinforce the catch. Maybe I didn't reinforce the catch
  enough... Regardless, I really like the Sidestrike, and I don't find
  that many used (or new), so I really don't wanna risk breaking it.

- Strongarm: Several people in Nerf forums complain that it breaks
  easily after upgrading the spring. I replaced the stock spring with a
 fFirestrike spring, didn't have to upgrade the catch, and it improved
  performance (I have no chronograph to measure improvement). I did had
  to trim the plastic tabs near the plunger head on the plunger rod for
  the Firestrike spring to fit.
