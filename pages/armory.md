<!-- 1 MENU_ENTRY=Armory -->
# Armory



## Primaries

- X-Shot Turbo Advance (with an added strongarm, quite powerful)

- Nerf Elite Rampage (stock internals)

- Nerf Modulus Tri-strike (with all attachments and stock internals)

- ~~Nerf Mega Rotofury~~ (gutted for parts after breaking the trigger
  spring)



## Secondaries

- Nerf Elite Roughcut (X2, stock internals)

- Nerf Elite Strongarm (drop clip mod, running a firestrike spring)

- Nerf Rival Kronos (red team version, stock internals)



## Tertiaries/Single-Shots

- Nerf Zombiestrike Sidestrike (ar removed)

- Nerf Elite Firestrike (elite blue, ar removed, running on a cut down rotofury spring)

- Nerf Elite Firestrike (orange, stock internals)

- Nerf Dart Tag Sharpshot (blue trigger, ar removed)

- Nerf Reflex IX-1 (ar removed)

- Nerf Elite Jolt (ar removed, orange trigger, bottom panel damaged on
  screw holes)

- Nerf microshots Flipfury

- Nerf Elite 2.0 Ace SD-1



## Mags

- 18-dart stick mag (official nerf, opened up and lubed, some damage in
  the process)

- 12-dart stick mag (official nerf)

- 10-dart stick mag (official nerf)

- Strongarm cylinder (X3)

- X-Shot 40 cylinder drum (included in the Turbo Advance)


## Ammo

- X-Shot waffle head darts (< 50 opened, 50-dart bag sealed)

- X-Shot 200-dart crate (old variant, fewer than 200)

- Random assortment of darts

- 10 Mega darts (one really old and performing subpar)

- 4 "demolisher" rockets



## Gear

- Adult-sized eye protection (X3 with smoked lenses, X1 lab goggles)

- Child-sized eye protection (X5, clear lenses, still fits adult faces)

- Handmade paracord bandoleer (snake knot)

- Handmade quick deploy "zipper" pull to attach a small secondary to
  a belt loop or bandoleer

- Random assortment of handmade paracord "zipper" pulls to hang on
  handle loops, or sling mounts. Useful to expand sling mounts.
